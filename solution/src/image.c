#include "../include/image.h"

struct pixel *get_pixel(uint32_t x, uint32_t y, struct image const *img) {
    return img->data + x + y * img->width;
}

struct image construct_image(uint64_t width, uint64_t height) {
    void *data = malloc(width * height * sizeof(struct pixel));
    if (data == NULL) {
      return (struct image) {0};
    }
    return (struct image) {
            width, height, data
    };
}

void destruct_image(struct image *const img) {
    if (img == NULL) {
        return;
    }
    img->width = 0;
    img->height = 0;
    free(img->data);
    img->data = NULL;
}
