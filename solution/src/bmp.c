#include "../include/bmp.h"
#include <stdint.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t calculate_padding(uint32_t width) {
    return PADDING - (width * sizeof(struct pixel)) % PADDING;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL || img == NULL) {
        return BMP_READ_ERROR;
    }
    struct bmp_header header;
    if (fread(&header, HEADER_SIZE, 1, in) != 1) {
        return BMP_READ_ERROR;
    }
    *img = construct_image(header.biWidth, header.biHeight);
    if (img->width == 0) {
      destruct_image(img);
      return BMP_READ_ERROR;
    }
    for (uint32_t y = 0; y < img->height; y++) {
        if (fread(get_pixel(0, y, img), sizeof(struct pixel), img->width, in) != img->width ||
            fseek(in, calculate_padding(img->width), SEEK_CUR) != 0) {
              destruct_image(img);
              return BMP_READ_ERROR;
        }
    }
    return BMP_READ_OK;
}

static struct bmp_header construct_bmp_header(uint32_t width, uint32_t height) {
    const uint32_t image_size = (sizeof(struct pixel) * width + calculate_padding(width)) * height;
    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = HEADER_SIZE + image_size,
            .bfReserved = RESERVED,
            .bOffBits = HEADER_SIZE,
            .biSize = INFO_HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = PLANES_NUMBER,
            .biBitCount = BITS_COUNT,
            .biCompression = NO_COMPRESSION,
            .biSizeImage = image_size,
            .biXPelsPerMeter = PPX,
            .biYPelsPerMeter = PPX,
            .biClrUsed = COLORS,
            .biClrImportant = COLORS
    };
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    if (out == NULL || img == NULL) {
        return BMP_WRITE_ERROR;
    }
    const struct bmp_header header = construct_bmp_header(img->width, img->height);
    if (fwrite(&header, HEADER_SIZE, 1, out) != 1) {
        return BMP_WRITE_ERROR;
    }
    uint8_t padding = calculate_padding(img->width);
    char padding_array[PADDING] = {0};
    for (uint32_t y = 0; y < img->height; y++) {
        if (fwrite(get_pixel(0, y, img), sizeof(struct pixel), img->width, out) != img->width ||
                fwrite(padding_array, sizeof(char), padding, out) != padding) {
            return BMP_WRITE_ERROR;
        }
    }
    return BMP_WRITE_OK;
}
