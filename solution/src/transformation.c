#include "../include/transformation.h"

struct image rotate(struct image const source) {
    struct image result = construct_image(source.height, source.width);
    if (result.width == 0) {
        destruct_image(&result);
        return result;
    }
    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            *get_pixel(source.height - 1 - y, x, &result) = *get_pixel(x, y, &source);
        }
    }
    return result;
}
