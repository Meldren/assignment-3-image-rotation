#include "../include/bmp.h"
#include "../include/localization.h"
#include "../include/transformation.h"
#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        print_error(ARGUMENTS_ERROR);
        return -1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        print_error(OPEN_FILE_ERROR);
        return -1;
    }
    struct image source = {0};
    enum read_status read_status = from_bmp(in, &source);
    if (fclose(in) == EOF) {
        print_error(CLOSE_FILE_ERROR);
    }
    if (read_status == BMP_READ_ERROR) {
        print_error(READ_ERROR);
        return -1;
    }
    print(READ_OK);
    struct image result = rotate(source);
    destruct_image(&source);

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL) {
        destruct_image(&result);
        print_error(OPEN_FILE_ERROR);
        return -1;
    }
    enum write_status write_status = to_bmp(out, &result);
    destruct_image(&result);
    if (fclose(out) == EOF) {
        print_error(CLOSE_FILE_ERROR);
    }
    if (write_status == BMP_WRITE_ERROR) {
        print_error(WRITE_ERROR);
        return -1;
    }
    print(WRITE_OK);
    return 0;
}
