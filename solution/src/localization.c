#include "../include/localization.h"

void print(const enum message_id id) {
    fprintf(stdout, "%s\n", strings[id]);
}

void print_error(const enum message_id id) {
    fprintf(stderr, "%s\n", strings[id]);
}
