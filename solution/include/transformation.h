#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image source);

#endif
