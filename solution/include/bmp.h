#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

#define PADDING 4
#define TYPE 0x4D42
#define HEADER_SIZE 54
#define INFO_HEADER_SIZE 40
#define RESERVED 0
#define PLANES_NUMBER 1
#define BITS_COUNT 24
#define NO_COMPRESSION 0
#define PPX 0
#define COLORS 0

enum read_status {
    BMP_READ_OK = 0,
    BMP_READ_ERROR
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status {
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image const *img);

#endif
