#ifndef LOCALIZATION_H
#define LOCALIZATION_H

#include <stdio.h>

enum message_id {
    READ_OK = 0,
    READ_ERROR,
    WRITE_OK,
    WRITE_ERROR,
    OPEN_FILE_ERROR,
    CLOSE_FILE_ERROR,
    ARGUMENTS_ERROR
};

static const char *strings[] = {
        [READ_OK] = "Исходное изображение было успешно просканировано.",
        [READ_ERROR] = "Ошибка при сканировании исходного изображения.",
        [WRITE_OK] = "Повернутое изображение было успешно создано :3",
        [WRITE_ERROR] = "Не удалось создать повернутое изображение.",
        [OPEN_FILE_ERROR] = "Не удалось открыть файл.",
        [CLOSE_FILE_ERROR] = "Не удалось закрыть файл.",
        [ARGUMENTS_ERROR] = "Вы указали неверное количество аргументов.\nСинтаксис: <Исходное_изображение> <Повернутое_изображение>"
};

void print(enum message_id id);

void print_error(enum message_id id);

#endif
